package com.interview.bongobd.myapplication;

import android.app.Activity;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class MainActivity extends Activity implements TouchGestureDetection.SimpleGestureListener {

    VideoView videoView;
    MainActivity activity;
    MediaController mediaController;
    AudioManager audioManager;
    TouchGestureDetection detector;
    int currentPosition;
    int currentVolume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        videoView = (VideoView) findViewById(R.id.videoView);
        detector = new TouchGestureDetection(this, this);
        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);

        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.samplevideo2);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(uri);
        videoView.requestFocus();
        videoView.start();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        this.detector.onTouchEvent(motionEvent);
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override
    public void onSwipe(int direction) {
        
        String msg = "";

        switch (direction) {

            case TouchGestureDetection.SWIPE_LEFT:

                currentPosition = videoView.getCurrentPosition();
                currentPosition = videoView.getCurrentPosition() + 10000;
                videoView.seekTo(currentPosition);
                msg = "Forward";
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                break;

            case TouchGestureDetection.SWIPE_RIGHT:

                currentPosition = videoView.getCurrentPosition();
                currentPosition = videoView.getCurrentPosition() - 10000;
                videoView.seekTo(currentPosition);
                msg = "Rewind";
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
                break;

        }

    }

}